import { tap, expect } from '@pushrocks/tapbundle';
import { Subject } from 'rxjs';

import * as smartcli from '../ts/index.js';

tap.test('should create a new Smartcli', async () => {
  const smartCliTestObject = new smartcli.Smartcli();
  expect(smartCliTestObject).toBeInstanceOf(smartcli.Smartcli);
});

tap.test('should add an command', async (toolsArg) => {
  const done = toolsArg.defer();
  const smartCliTestObject = new smartcli.Smartcli();
  const awesomeCommandSubject = smartCliTestObject.addCommand('awesome')
  expect(awesomeCommandSubject).toBeInstanceOf(Subject);
  awesomeCommandSubject.subscribe(() => {
    done.resolve();
  });
  console.log(process.argv);
  process.argv.splice(2, 0, 'awesome');
  console.log(process.argv);
  smartCliTestObject.startParse();
  await done.promise;
});

tap.test('should start parsing a standardTask', async () => {
  const smartCliTestObject = new smartcli.Smartcli();
  expect(smartCliTestObject.standardCommand()).toBeInstanceOf(Subject);
});

let hasExecuted: boolean = false;

tap.test('should accept a command', async () => {
  const smartCliTestObject = new smartcli.Smartcli();
  smartCliTestObject.addCommand('triggerme').subscribe(() => {
    hasExecuted = true;
  });
  smartCliTestObject.triggerCommand('triggerme', {});
  expect(hasExecuted).toBeTrue();
});

tap.start();
