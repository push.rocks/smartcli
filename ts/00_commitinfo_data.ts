/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartcli',
  version: '4.0.6',
  description: 'easy observable cli tasks'
}
